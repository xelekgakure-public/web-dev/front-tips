
# DOCKER COMMANDS

up:
	docker compose up -d

build:
	docker compose build

full-up:
	docker compose up -d --build

down:
	docker compose down
