(() => {
    /* ---------------------------- */
    /* Alerts ---------------------- */

    const AlertList = document.getElementById('alerts')
    const API = "http://127.0.0.1:8000" // Etant donnée que le site tourne sur docker en local, on utilise `http://127.0.0.1:8000`

    /* ---------------------------- */
    /* Components ---------------------- */

    const MyItem = (item) => {
        const reaction = (e) => {
            console.log(e.target.getAttribute('dataId'))
        }
    
        const deleteHello = (e) => {
            const dataId = e.target.getAttribute('dataId')
            console.log("DELETE", dataId)
            fetch(API + '/item/' + dataId, {method: 'DELETE'})
                .then(res => res.json())
                .then(res => document.getElementById(`hello-${dataId}`).remove())
        }

        const DeleteItem = (id) => {
            const newDeleteItem = document.createElement('button')
            newDeleteItem.id = `hello-${id}--remove`
            newDeleteItem.setAttribute('dataId', id)
            newDeleteItem.className = 'my-item-random--remove'
            newDeleteItem.innerHTML = "X"
            newDeleteItem.addEventListener('click', deleteHello)
            return newDeleteItem
        }

        const newItem = document.createElement('div')
        newItem.id = `hello-${item.id}`
        newItem.setAttribute('dataId', item.id)
        newItem.className = 'my-item-random'
        newItem.innerHTML = item.name
        newItem.addEventListener('click', reaction)
        newItem.appendChild(DeleteItem(item.id))
        return newItem
    }

    const Alert = (title=null, message="A new alert") => {
        const alertItem = document.createElement('div')
        const alertBody = document.createElement('div')

        alertItem.className = 'alert'
        alertBody.className = 'alert__body'

        if (title) {
            const alertTitle = document.createElement('div')
            alertTitle.className = 'alert__title'
            alertTitle.innerHTML = title
            alertItem.appendChild(alertTitle)
            alertItem.appendChild(document.createElement('hr'))
        }

        alertBody.innerHTML = message
        alertItem.appendChild(alertBody)

        return alertItem
    }

    /* ---------------------------- */
    /* Services ------------------- */

    const loadItems = () => {
        document.querySelector('#my-app').innerHTML="<span>Loading...</span>"
        fetch(API + '/item')
            .then(res => res.json())
            .then(res => {
                console.log(res)
                document.querySelector('#my-app').innerHTML=""
                res.forEach(item => {
                    document.querySelector('#my-app').appendChild(MyItem(item))
                    console.log(item)
                })       
        })
    }

    const formSubmit = (e) => {
        e.preventDefault()
        const name = document.getElementById('my-new-item-name')
        if (!name.value.match(/^test_/)) {
            alert("Name should begin with 'test_'")
            return
        }

        fetch(API + "/item", {
            method: "POST", 
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({name: name.value})
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    AlertList.appendChild(Alert("Item error", res.error))
                    return
                }
                loadItems()
            })
    }

    const formChange = (e) => {
        /**
         * Cette fonction permet de vérifier pour un formulaire donné si tout
         * ses champs requis ont une valeur afin d'activer ou désactiver le
         * boutton "submit"
         */
        const btn = e.currentTarget.querySelector('button[type="submit"]')

        for (const input of e.currentTarget.querySelectorAll('input[required]')) {
            if (input.value === "") {
                btn.setAttribute('disabled', 'disabled')
                return
            }
        }
        btn.removeAttribute('disabled')
    }


    document.getElementById('my-form').addEventListener('submit', formSubmit)
    document.getElementById('my-form').addEventListener('change', formChange)
    loadItems()
})()