class ModalObject {
    _shown = false
    _node = null

    id = ""
    props = null

    body = ""
    title = ""
    footer = ""

    content = ""

    constructor (object) {
        this.props = object.attributes
        this.id = this.props.id
        this._shown = (this.props.hidden == undefined || this.props.hidden === "false")
        this._generate(object)
    }

    _link = () => {
        document.querySelectorAll(`.modalButton[showModal="${this.id.value}"]`).forEach(item => {item.addEventListener('click', this.show)})
        document.querySelectorAll(`.modalButton[hideModal="${this.id.value}"]`).forEach(item => {item.addEventListener('click', this.hide)})
        document.querySelectorAll(`.modalButton[toggleModal="${this.id.value}"]`).forEach(item => {item.addEventListener('click', this.toggle)})
    }

    _generate = (object) => {
        this.content = object.innerHTML
        this.title = object.querySelector('ModalTitle')?.innerHTML ?? null
        this.body = object.querySelector('ModalBody')?.innerHTML ?? null
        this.footer = object.querySelector('ModalFooter')?.innerHTML ?? null

        console.log(this._node, this.props.id.value)
        document.querySelector("Modal[id=" + this.props.id.value + "]").remove()
        this._node = document.createElement('div')
        this._node.id = this.id.value
        this._node.className = "modal"
        this._node.appendChild(this._build())
        this._node.addEventListener('click', e => { if (e.currentTarget === e.target) this.hide(e) })
        document.body.appendChild(this._node)
        this._link()
        this.hide()
    }

    _build = () => {
        const ModalDiv = document.createElement('div')
        ModalDiv.className = "modal-content"

        if (this.title !== null) {
            const Title = document.createElement('div')
            Title.innerHTML = this.title
            Title.className = "modal-title"
            ModalDiv.appendChild(Title)
        }

        const Body = document.createElement('div')
        Body.innerHTML = this.body ?? this.content
        Body.className = "modal-body"
        ModalDiv.appendChild(Body)

        if (this.footer !== null) {
            const Footer = document.createElement('div')
            Footer.innerHTML = this.footer
            Footer.className = "modal-footer"
            ModalDiv.appendChild(Footer)
        }

        return ModalDiv
    }

    getElement = () => {
        return document.querySelector("#" + this.id.value)
    }

    show = (e) => {
        this._shown = true
        console.log('show modal')
        this.getElement().style.display = 'block'
    }

    hide = (e) => {
        this._shown = false
        console.log('hide modal')
        this.getElement().style.display = 'none'
    }

    toggle = (e) => {
        this._shown = !this._shown
        console.log('toggle modal')
        this.getElement().style.display = this._shown ? 'block' : 'none'
    }

    create = (e) => {

    }

    destroy = (e) => {

    }
}


export const Modal = {
    init: () => {
        const modals = document.querySelectorAll("Modal")
        modals.forEach(modal => {
            const obj = new ModalObject(modal)
            console.log(document.querySelectorAll(`.modalButton[showModal=${modal.id}]`))
        })
    }
}