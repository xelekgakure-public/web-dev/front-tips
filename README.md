# Web development tips

_Petite stakc faite pour donner quelques exemples de développement web_

| SERVICE | TECHNO         | PORT |
| ------- | -------------- | ---- |
| Front   | HTML/CSS/JS    | 8001 |
| API     | Python/FastApi | 8000 |

## Lancer l'application

Pour faire asser simple, j'ai créé un `Makefile`, mais si vous êtes familier avec `docker` n'hésitez pas à le lancer vous même avec `docker compose up -d`

---

## Services
### API 

Petite api rapide développée avec le Framework FastAPI.
Système de gestion des données custom utilisant un simple fichier JSON pour le stockage.

Accès : [http://127.0.0.1:8000]

Routes:

#### GET `/`
Retourne un JSON contenant: `{"Hello": "World"}`

#### GET `/item`
Retourne un JSON contenant le listing des items renseignés dans le fichier `current_data.json`

#### POST `/item`
Crée un item et l'ajoute au fichier `current_data.json`

#### GET `/item/{item_id}`
Retourne les donnée contenue dans `current_data.json` correspondant à l'id renseigné à la place de `{item_id}`

#### DELETE `/item/{item_id}`
Supprime l'item correspondant à l'id `{item_id}` dans le fichier fichier `current_data.json`


### Front
Site web basique n'ayant pas de style particulier, montrant juste certaines fonctionnalitées de façon fonctionnelle.