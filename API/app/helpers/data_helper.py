class DataHelper:
    class Data:
        id: int
        name: str
    
    _data: list[Data] = []
    
    def find(self, **kwargs):
        items = []
        for arg in kwargs:
            for data in self._data:
                if data.__getattribute__(kwargs.keys()[0]) == kwargs[0]:
                    items.append(data)                
        return items