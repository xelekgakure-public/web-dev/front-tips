import json
import shutil

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import HTTPException

from typing import Union
from pathlib import Path
from pydantic import BaseModel


class Item(BaseModel):
    name: str


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

DATA_FILE = Path(__file__).parent / 'resources/data.json'
CURRENT_DATA_FILE = Path(__file__).parent / 'resources/current_data.json'
DATA: list = []

shutil.copy(DATA_FILE, CURRENT_DATA_FILE)

with open(CURRENT_DATA_FILE, 'r', encoding='utf-8') as data_fs:
    DATA = json.loads(data_fs.read())


@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.post('/item')
def add_item(new_item: Item):
    next_id = 0
    for item in DATA:
        if next_id <= item.get('id'):
            next_id = item.get('id') + 1
        if item.get('name') == new_item.name:
            return {"error": "Data already exists", "result": item}
    with open(CURRENT_DATA_FILE, 'w+', encoding='utf-8') as data_fs:
        DATA.append({'id': next_id, 'name': new_item.name})
        data_fs.write(json.dumps(DATA))
    return new_item.model_dump()


@app.get("/item")
def read_item(q: Union[str, None] = None):
    return DATA


@app.get("/item/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


@app.delete("/item/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    for item in DATA:
        if item.get('id') == item_id:
            del DATA[DATA.index(item)]
            with open(CURRENT_DATA_FILE, 'w+', encoding='utf-8') as data_fs:
                data_fs.write(json.dumps(DATA))
            break
    return {"item_id": item_id, "DELETED": True}
